<?php
// $Id: block.tpl.php,v 1.3 2007/08/07 08:39:36 goba Exp $
?>
  <div class="block block-<?php print $block->module; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>">
  	<div class="outer">
  		<div class="inner">
			<?php print $block->content; ?>
		</div>
	</div>
 </div>
