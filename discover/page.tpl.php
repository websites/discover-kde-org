<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"></script>
</head>
<body>
<div id="backdrop">
	<div id="backdrop-bottom">
	<div id="container">
		<div id="header">
			<div id="header-title"><a href="/" title="Home"><img src="themes/discover/images/header-title.png" alt="Discover KDE"/></a></div>
			<div id="top-menu">
			<?php if (isset($primary_links))
			{
				print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist'));
			}?>
			</div>
		</div>
		<div id="middle">
			<div id="left">
				<div class="block">
					<div class="outer">
						<div class="inner">
							<?php print $breadcrumb ?>
							<h1 class="title"><?php print $title ?></h1>
							<div class="tabs"><?php print $tabs ?></div>
							<?php if ($show_messages) { print $messages; } ?>
							<?php print $help ?>
							<?php print $content; ?>
							<?php print $feed_icons; ?>
						</div>
					</div>
				</div>
		  	</div>
		  	<?php if(isset($right)){
		  		print "<div id=\"right\">";
		  		print $right;
		  		print "</div>";
		  		}
			?>
			<div class="clear"></div>
		</div>
		<div id="bottom">
			<div id="footer">
				  <?php print $footer_message ?>
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>
